# webkit2gtk5-rs

The Rust bindings of [webkit2gtk5](https://webkitgtk.org/)

Website: <https://world.pages.gitlab.gnome.org/Rust/webkit2gtk5-rs>

## Documentation

- webkit2gtk5: <https://world.pages.gitlab.gnome.org/Rust/webkit2gtk5-rs/stable/latest/docs/webkit2gtk5>
- webkit2gtk5-sys: <https://world.pages.gitlab.gnome.org/Rust/webkit2gtk5-rs/stable/latest/docs/webkit2gtk5_sys>
- webkit2gtk-webextension: <https://world.pages.gitlab.gnome.org/Rust/webkit2gtk5-rs/stable/latest/docs/webkit2gtk_webextension>
- webkit2gtk-webextension-sys: <https://world.pages.gitlab.gnome.org/Rust/webkit2gtk5-rs/stable/latest/docs/webkit2gtk_webextension_sys>
- javascriptcore5: <https://world.pages.gitlab.gnome.org/Rust/webkit2gtk5-rs/stable/latest/docs/javascriptcore>
- javascriptcore5-sys: <https://world.pages.gitlab.gnome.org/Rust/webkit2gtk5-rs/stable/latest/docs/javascriptcore5_rs_sys>